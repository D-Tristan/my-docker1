Commandes à faire :


sudo apt-get install python3.7-venv
sudo python3.7 -m venv .venv

-> cela crée le dossier .venv



Acceder à l'environnement virtuel :
- source ./.venv/bin/activate               cela met un "(.venv)" devant le path

Remarque : de base PyCharm a crée le projet avec un dossier venv, je l'ai supprimé pour créer le .venv.
Les deux notations marchent, quel que soit le nom c'est ok, mais c'est mieux d'avoir le nom .venv car cela diffère
du nom du module.



